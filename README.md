Bileto
======
Bileto es un software de gestión de incidencias escrito en Python3 y PyQt5.

Instalación
===========
* Ubuntu
```bash
sudo apt-get install python3-pyqt5 pyqt5-dev pyqt5-dev-tools
```

* Fedora

seguir las instrucciones de: http://robertbasic.com/blog/install-pyqt5-in-python-3-virtual-environment

* Arch Linux
```bash
sudo pacman -S python-pyqt5
```

Uso
===
```bash
git clone git clone https://LordFeratum@bitbucket.org/LordFeratum/bileto.git
cd bileto
pip install -i requirements.txt
ipython src/bileto.py
```

Crear nueva base de datos
=========================
```bash
cd bileto
rm bileto.db
ipython src/storage/db.py create
```
