# -*- coding: utf-8 -*-
__author__ = 'Enrique Piña Monserrat'


from sys import exit

from PyQt5.QtWidgets import *

from src.utils.exceptions import UserNotFoundException

from src.gui.windows.window_login import LoginDialog
from src.gui.windows.window_main import MainWindow

from src.storage.model import User
from src.storage.session import alchemy_ss, alchemy_ss_tx


@alchemy_ss_tx
def create_new_user(user_data, ss=None):
    username  = user_data.get('user')
    fullname  = user_data.get('fullname')
    password  = user_data.get('pwd')
    role      = user_data.get('role')
    extern    = user_data.get('extern')
    knowledge = user_data.get('knowledge')

    user           = User()
    user.name      = username
    user.fullname  = fullname
    user.password  = password
    user.role      = role
    user.extern    = extern
    user.knowledge = knowledge
    ss.add(user)

    return username, fullname


@alchemy_ss
def valid_user(user_data, ss=None):
    username = user_data.get('user')
    password = user_data.get('pwd')

    user = ss.query(User)\
             .filter(User.name==username,
                     User.password==password)

    user_count = user
    if user_count.count() > 0:
        u = user.first()
        return u.name, u.fullname

    else:
        raise UserNotFoundException("El usuario '%s' no se encuentra en nuestra base de datos." % username)


def validate_user(user_data):
    valid = True
    name  = None
    fname = None

    if 'role' in user_data.keys():
        name, fname = create_new_user(user_data)
        return name, fname

    else:
        try:
            name, fname = valid_user(user_data)
            return name, fname

        except UserNotFoundException as e:
            raise e


if __name__ == '__main__':
    app = QApplication([])
    invalid = True

    while invalid:
        user_data   = LoginDialog.getInfo()

        try:
            name, fname = validate_user(user_data)
            win = MainWindow(user=[name])
            invalid = False

        except UserNotFoundException as e:
            QMessageBox.critical(None, 'Usuario invalido', e.message)


    exit(app.exec_())
