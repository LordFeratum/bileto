import sys

from sqlalchemy import create_engine, MetaData

from src.storage.model import Base as model_base


def get_engine():
    return create_engine('sqlite:///bileto.db')


def create():
    model_base.metadata.create_all(get_engine())


if __name__ == '__main__':
    if len(sys.argv) == 2 and sys.argv[1] == 'create':
        create()
        print('>> Database has been created')

    else:
        print('>> I need more info for do anything')
