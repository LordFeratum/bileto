from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Float, Boolean, DateTime, Integer, SmallInteger, Date, text, Sequence, Boolean
from sqlalchemy.types import TypeDecorator, CHAR
from sqlalchemy.orm import relationship
from sqlalchemy.schema import ForeignKey


reverse_dict = lambda d: dict(zip(d.values(), d.keys()))

SCAL_TYPES      = {1: 'Básico', 2: 'Experto', 3: 'Externo'}
R_SCAL_TYPES    = reverse_dict(SCAL_TYPES)

USER_TYPES      = {1: 'Administrador', 2: 'Programador', 3: 'Cliente'}
R_USER_TYPES    = reverse_dict(USER_TYPES)

TICKET_STATUS   = {1: 'Inicio', 2: 'Asignado', 3: 'En Proceso', 4: 'Cerrado'}
R_TICKET_STATUS = reverse_dict(TICKET_STATUS)

TICKET_PRIOR    = {1: 'Crítica', 2: 'Grave', 3: 'Alta', 4: 'Media', 5: 'Baja'}
R_TICKET_PRIOR  = reverse_dict(TICKET_PRIOR)

KNOWLEDGE       = {1: 'Básicos', 2: 'Avanzados'}
R_KNOWLEDGE     = reverse_dict(KNOWLEDGE)


Base = declarative_base()

class User(Base):
    __tablename__ = 'users'
    pid = Column(Integer, Sequence('user_id_seq'), primary_key=True)

    name      = Column(String(50))
    fullname  = Column(String)
    password  = Column(String(12))

    role      = Column(Integer)
    knowledge = Column(Integer)
    extern    = Column(Boolean)

    def get_role(self): return USER_TYPES.get(self.role, 'Administrator')
    def set_role(self, r): self.role = R_USER_TYPES.get(r, 1)

    def is_cliente(self):
        return self.role == R_USER_TYPES.get('Cliente')

    def is_externo(self):
        return self.extern

    def get_knowledge(self):
        return KNOWLEDGE.get(self.knowledge)

    def __unicode__(self):
        return "%s" % self.fullname

    def __str__(self):
        return "%s" % self.fullname

    def __repr__(self):
        return '%s' % self.fullname


class Ticket(Base):
    __tablename__ = 'tickets'
    pid = Column(Integer, Sequence('pass_id_seq'), primary_key=True)

    created      = Column(DateTime)
    description  = Column(String)

    priority     = Column(Integer)
    status       = Column(Integer)
    scalability  = Column(Integer)

    owner_pid    = Column(Integer, ForeignKey('users.pid'))
    owner        = relationship(User, primaryjoin=owner_pid==User.pid)

    assigned_pid = Column(Integer, ForeignKey('users.pid'))
    assigned     = relationship(User, primaryjoin=assigned_pid==User.pid)

    def set_priority(self, p): self.priority = R_TICKET_PRIOR.get(p, 1)
    def get_priotity(self)   : return TICKET_PRIOR.get(self.priority, 'Crítica')

    def set_status(self, s): self.status = R_TICKET_STATUS.get(s, 1)
    def get_status(self)   : return TICKET_STATUS.get(self.status, 'Inicio')


class Comment(Base):
    __tablename__ = 'comments'
    pid = Column(Integer, Sequence('comments_id_seq'), primary_key=True)

    created    = Column(DateTime)
    text       = Column(String)

    owner_pid  = Column(Integer, ForeignKey('users.pid'))
    owner      = relationship(User, primaryjoin=owner_pid==User.pid)

    ticket_pid = Column(Integer, ForeignKey('tickets.pid'))
    ticket     = relationship(Ticket, primaryjoin=ticket_pid==Ticket.pid)








