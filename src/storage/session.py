from traceback import format_exc

from sqlalchemy.orm import sessionmaker

from src.storage.db import get_engine


def session():
    session = sessionmaker()
    session.configure(bind=get_engine())
    return session()


def alchemy_ss(func):
    def _wrapper(*args, **kwargs):
        has_ss=True
        if ("ss" in kwargs and kwargs.get("ss") == None) or (not "ss" in kwargs):
            kwargs["ss"] = session()
            has_ss = False
        res = func(*args, **kwargs)
        if not has_ss: kwargs["ss"].close()
        return res
    return _wrapper


def alchemy_ss_tx(func):
    def _wrapper(*args, **kwargs):
        has_ss = True
        if ("ss" in kwargs and kwargs.get("ss") == None) or (not "ss" in kwargs):
            ss = session()
            kwargs["ss"] = ss
            has_ss = False
        else:
            ss = kwargs["ss"]
        try:
            res = func(*args, **kwargs)
            ss.commit()
            return res
        except Exception as e:
            ss.rollback()
            print(format_exc(e))
            raise e
        finally:
            if not has_ss:
                ss.close()
    return _wrapper
