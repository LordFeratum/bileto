from functools import partial
from threading import Timer

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from src.gui.forms.insert_user_page import Ui_Form
from src.storage.session import alchemy_ss
from src.storage.model import User
from src.storage.model import R_USER_TYPES


SAVE, SAVE_AND_EXIT = range(2)
BAD_MESSAGE, GOOD_MESSAGE = range(2)

class InsertUserPage(QWidget):
    def __init__(self,  parent=None):
        super(InsertUserPage, self).__init__(parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self._hide_messages()
        self.ui.cmbRole.addItems(R_USER_TYPES.keys())

        self.ui.btnSave.released.connect(partial(self._save, SAVE))
        self.ui.btnSaveAndExit.released.connect(partial(self._save, SAVE_AND_EXIT))

    def _hide_messages(self):
        self.ui.lblBad.setVisible(False)
        self.ui.lblGood.setVisible(False)

    def _show_message(self, msg, mtype):
        lbl = self.ui.lblGood if mtype==GOOD_MESSAGE else self.ui.lblBad

        lbl.setVisible(True)
        lbl.setText(msg)

        Timer(5, self._hide_messages).start()

    @alchemy_ss
    def _save(self, mode, ss=None):
        def _validate(user):
            ucount = ss.query(User)\
                       .filter(User.name==user)\
                       .count()

            return ucount==0

        def _create(fname, user, pwd, role):
            u = User(name=user, fullname=fname, password=pwd, role=role)
            ss.add(u)
            ss.commit()

        fname = self.ui.txtNewFName.text()
        user  = self.ui.txtNewUser.text()
        pwd   = self.ui.txtNewPass.text()
        role  = R_USER_TYPES.get(self.ui.cmbRole.currentText())

        try:
            if _validate(user):
                _create(fname, user, pwd, role)
                self._show_message('Se ha insertado correctamente el usuario %s.' % user, GOOD_MESSAGE)
                self.ui.txtNewFName.setText('')
                self.ui.txtNewUser.setText('')
                self.ui.txtNewPass.setText('')
                self.ui.txtNewPass2.setText('')
                self.ui.cmbRole.setCurrentIndex(0)

            else:
                self._show_message('Ya existe un usuario con el nombre %s.' % user, BAD_MESSAGE)

        except Exception as e:
            self._show_message('ERROR: %s.' % e, BAD_MESSAGE)

