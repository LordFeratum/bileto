# -*- coding: utf-8 -*-


from PyQt5.Qt import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

from src.storage.model import *
from src.storage.session import *

from src.gui.forms.login import Ui_LogInDialog


class LoginDialog(QDialog):
    def __init__(self, parent=None):
        super(LoginDialog, self).__init__(parent)
        self.ui = Ui_LogInDialog()
        self.ui.setupUi(self)

        self._registering = False

        # Set initial status for graphical purpouses
        self.ui.groupBox_4.setVisible(False)
        self.ui.widget.setVisible(True)

        self.ui.cmbUserType.addItems(R_USER_TYPES.keys())
        self.ui.cmbKnowledge.addItems(R_KNOWLEDGE.keys())

        self.ui.btnBack.setEnabled(False)

        # Slots
        self.ui.btnRegister.released.connect(self.btn_register_released)
        self.ui.btnBack.released.connect(self.btn_back_released)
        self.ui.buttonsBox.accepted.connect(self.accept)
        self.ui.buttonsBox.rejected.connect(self.reject)
        self.ui.chkExtern.stateChanged.connect(self.chk_changed)

    def chk_changed(self, idx):
        visible = self.ui.chkExtern.isChecked()
        self.ui.lblConocimientos.setVisible(not visible)
        self.ui.cmbKnowledge.setVisible(not visible)

    def btn_register_released(self):
        self._registering = True
        self.ui.widget_3.setVisible(False)
        self.ui.groupBox_4.setVisible(True)
        self.ui.widget.setVisible(False)

        self.ui.btnBack.setEnabled(True)

    def btn_back_released(self):
        self.ui.txtNewPass.setText('')
        self.ui.txtNewUSer.setText('')
        self.ui.txtFullname.setText('')

        self.ui.widget.setVisible(True)
        self.ui.widget_3.setVisible(True)
        self.ui.groupBox_4.setVisible(False)

        self.ui.btnBack.setEnabled(False)

    def get_info(self):
        user   = self.ui.txtNewUSer.text() if self._registering else self.ui.txtLoginUser.text()
        pwd    = self.ui.txtNewPass.text() if self._registering else self.ui.txtLoginPass.text()
        fnm    = self.ui.txtFullname.text()
        utype  = R_USER_TYPES.get(self.ui.cmbUserType.currentText())
        extern = self.ui.chkExtern.isChecked()
        know   = R_KNOWLEDGE.get(self.ui.cmbKnowledge.currentText())

        return user, pwd, fnm, utype, extern, know, self._registering

    @staticmethod
    def getInfo(parent=None):
        dialog = LoginDialog(parent)
        result = dialog.exec_()

        user, pwd, fullname, role, extern, knowledge, saving = dialog.get_info()

        data = dict(user=user, pwd=pwd, valid=result==QDialog.Accepted)
        if saving:
            data.update(dict(role=role, fullname=fullname, extern=extern, knowledge=knowledge))

        return data

