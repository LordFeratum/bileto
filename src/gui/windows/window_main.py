from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from utils.exceptions import UserNotFoundException

from storage.model import User, Ticket
from storage.session import alchemy_ss, alchemy_ss_tx

from gui.windows.wellcome_page import WellcomePage
from gui.windows.insert_ticket_page import InsertTicketPage
from gui.windows.view_ticket_page import ViewTicketPage
from gui.windows.insert_user_page import InsertUserPage
from gui.windows.manage_tickets_page import ManageTicketsPage

from gui.forms.main import Ui_MainWindow


class MainWindow(QMainWindow):
    def __init__(self, user, parent=None):
        super(MainWindow, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        try:
            self._user = self._load_user(user[0])

        except UserNotFoundException as e:
            print(e.message)

        # Pages
        self._welcome_page        = WellcomePage(self._user)
        self._insert_ticket_page  = InsertTicketPage(self._user)
        self._insert_user_page    = InsertUserPage()
        self._manage_tickets_page = ManageTicketsPage(self._user)
        self._view_ticket_page    = ViewTicketPage(self._user)
        self.ui.stkMain.addWidget(self._welcome_page)
        self.ui.stkMain.addWidget(self._insert_ticket_page)
        self.ui.stkMain.addWidget(self._manage_tickets_page)
        self.ui.stkMain.addWidget(self._view_ticket_page)
        self.ui.stkMain.addWidget(self._insert_user_page)

        # Init
        self.ui.stkMain.setCurrentWidget(self._welcome_page)

        # set first gui elements
        self._gui_logic()

        self.ui.btnUser.setVisible(False)
        self.ui.btnManagement.setVisible(False)

        self.ui.ticketOptions.setVisible(False)
        self.ui.userOptions.setVisible(False)

        self.ui.btnTicket.released.connect(self._ticket_options)
        self.ui.btnUser.released.connect(self._user_options)
        self.ui.btnManagement.released.connect(self._management_options)

        self.ui.btnHome.released.connect(self._go_home)

        self.ui.btnAddTicket.released.connect(self._add_ticket)
        self.ui.btnManageTicket.released.connect(self._manage_tickets)
        self.ui.btnAddUser.released.connect(self._add_user)

        # Page signals
        self._welcome_page.ticketSelected.connect(self._ticket_selected)
        self._manage_tickets_page.ticketSelected.connect(self._ticket_selected)

        self.showMaximized()

    def _gui_logic(self):
        self.ui.lblUsernameLoged.setText(self._user.fullname)
        print(self._user.is_cliente(), self._user.is_externo(), self._user.role)
        if self._user.is_externo():
            self.ui.btnAddTicket.setVisible(False)

        if self._user.is_externo() or self._user.is_cliente():
            self.ui.btnUser.setVisible(False)
            self.ui.btnManagement.setVisible(False)

    @alchemy_ss
    def _load_user(self, username, ss=None):
        user = ss.query(User).filter(User.name==username).first()
        if user is None:
            raise UserNotFoundException("El usuario '%s' no ha sido encontrado en nuestra base de datos." % username)

        return user

    @alchemy_ss
    def _load_ticket(self, ticket_pid, ss=None):
        ticket = ss.query(Ticket).filter(Ticket.pid==ticket_pid).one()
        return ticket

    def _go_home(self):
        self.ui.ticketOptions.setVisible(False)
        self.ui.userOptions.setVisible(False)
        self._welcome_page.recreate_tickets()
        self.ui.stkMain.setCurrentWidget(self._welcome_page)

    def _ticket_selected(self, ticket_pid):
        ticket = self._load_ticket(ticket_pid)
        self._view_ticket_page.set_ticket(ticket)
        self.ui.stkMain.setCurrentWidget(self._view_ticket_page)

    def _ticket_options(self):
        self.ui.userOptions.setVisible(False)
        self.ui.ticketOptions.setVisible(not self.ui.ticketOptions.isVisible())

    def _user_options(self):
        self.ui.ticketOptions.setVisible(False)
        self.ui.userOptions.setVisible(not self.ui.userOptions.isVisible())

    def _management_options(self):
        self.ui.ticketOptions.setVisible(False)
        self.ui.userOptions.setVisible(False)

    def _add_ticket(self):
        self._insert_ticket_page.reload_users()
        self.ui.stkMain.setCurrentWidget(self._insert_ticket_page)

    def _add_user(self):
        self.ui.stkMain.setCurrentWidget(self._insert_user_page)

    def _watch_ticket(self):
        print('watching tickets')

    def _manage_tickets(self):
        self._manage_tickets_page.count()
        self._manage_tickets_page.set_tickets()
        self.ui.stkMain.setCurrentWidget(self._manage_tickets_page)
