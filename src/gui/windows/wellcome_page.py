from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from src.gui.forms.wellcome_page import Ui_WelcomePage

from src.storage.session import alchemy_ss
from src.storage.model import Ticket, User
from src.storage.model import R_TICKET_STATUS


def ticket_handler(obj, prop):
    from src.storage.model import TICKET_STATUS, TICKET_PRIOR

    attr = getattr(obj, prop)

    if prop=='priority':
        return TICKET_PRIOR.get(attr)

    elif prop=='status':
        return TICKET_STATUS.get(attr)

    elif prop=='created':
        return attr.strftime('%d/%m/%Y  (%H:%M)')

    else:
        return attr


class WellcomePage(QWidget):
    ticketSelected = pyqtSignal(int)

    def __init__(self, user, parent=None):
        super(WellcomePage, self).__init__(parent)
        self.ui = Ui_WelcomePage()
        self.ui.setupUi(self)

        self._user = user

        self._configure_tables()
        self._fill_table()

    def _configure_tables(self):
        self.ui.tblTicketPendientes.doubleClicked.connect(self._item_clicked)

        columns = ['Identificador', 'Fecha', 'Creador', 'Prioridad', 'Estado']
        props   = ['pid', 'created', 'owner', 'priority', 'status']

        self.ui.tblTicketPendientes.set_handler(ticket_handler)
        self.ui.tblTicketPendientes.set_header_view(QHeaderView.Stretch)
        self.ui.tblTicketPendientes.set_columns(columns, props)

    def _item_clicked(self, model):
        if not self._user.is_cliente() or self._user.is_externo():
            row = model.row()
            ticket = self.ui.tblTicketPendientes.getObjectAtRow(row)
            self.ticketSelected.emit(ticket.pid)

    @alchemy_ss
    def _get_tickets(self, ntickets=15, ss=None):
        tickets = None
        if self._user.is_cliente() and not self._user.is_externo():
            tickets = ss.query(Ticket)\
                        .filter(Ticket.owner_pid==self._user.pid,
                                Ticket.status!=R_TICKET_STATUS.get('Cerrado'))
        else:
            tickets = ss.query(Ticket)\
                        .filter(Ticket.assigned_pid==self._user.pid,
                                Ticket.status!=R_TICKET_STATUS.get('Cerrado'))\
                        .limit(ntickets)

        return tickets

    def recreate_tickets(self):
        self.ui.tblTicketPendientes.clear_contents()
        self._fill_table()

    def _fill_table(self):
        tickets = self._get_tickets()
        for ticket in tickets:
            self.ui.tblTicketPendientes.addObject(ticket)
