from functools import partial
from operator import eq, gt, lt
from sqlalchemy import or_

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from src.gui.forms.manage_tickets_page import Ui_Form

from src.storage.session import alchemy_ss
from src.storage.model import User, Ticket
from src.storage.model import R_USER_TYPES
from src.storage.model import R_TICKET_STATUS, R_TICKET_PRIOR


BACK, NEXT = -1, 1
TICKETS_PER_PAGE = 50

DATETIME, ASSIGNED, OWNER, STATUS, PRIORITY = range(5)

def ticket_handler(obj, prop):
    from src.storage.model import TICKET_STATUS, TICKET_PRIOR

    attr = getattr(obj, prop)

    if prop=='priority':
        return TICKET_PRIOR.get(attr)

    elif prop=='status':
        return TICKET_STATUS.get(attr)

    elif prop=='created':
        return attr.strftime('%d/%m/%Y  (%H:%M)')

    else:
        return attr


class ManageTicketsPage(QWidget):
    ticketSelected = pyqtSignal(int)

    def __init__(self,  user, parent=None):
        super(ManageTicketsPage, self).__init__(parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self._user     = user
        self._page     = 0
        self._max_page = int(self._count() / TICKETS_PER_PAGE) - 1

        self._operators = {'>': gt, '<': lt, '=': eq}

        self._configure_tables()
        self._fill_filter_boxes()

        self.ui.wdgFiltro.setVisible(False)
        self.ui.dtDatetime.setDate(QDate.currentDate())

        self.ui.btnBack.released.connect(partial(self._btn_action, BACK))
        self.ui.btnNext.released.connect(partial(self._btn_action, NEXT))
        self.ui.pushButton.released.connect(self._btn_filter_released)
        self.ui.txtPage.editingFinished.connect(self._editing_finished)
        self.ui.txtSearch.editingFinished.connect(self.set_tickets)

        self.ui.cmbAssigned.currentIndexChanged.connect(self._filter_changed)
        self.ui.cmbOwner.currentIndexChanged.connect(self._filter_changed)
        self.ui.cmbPriority.currentIndexChanged.connect(self._filter_changed)
        self.ui.cmbStatus.currentIndexChanged.connect(self._filter_changed)
        self.ui.cmbFormatDate.currentIndexChanged.connect(self._filter_changed)
        self.ui.dtDatetime.dateTimeChanged.connect(self._filter_changed)

    @alchemy_ss
    def _get_filter(self, date_op, ticket_date, owner, assigned, prior, status, ss=None):
        ticket_query = ss.query(Ticket)

        if date_op is not None:
            ticket_query = ticket_query.filter(date_op(Ticket.created, ticket_date))

        if owner is not None:
            ticket_query = ticket_query.filter(Ticket.owner_pid==owner)

        if assigned is not None:
            ticket_query = ticket_query.filter(Ticket.assigned_pid==assigned)

        if prior is not None:
            ticket_query = ticket_query.filter(Ticket.priority==prior)

        if status is not None:
            ticket_query = ticket_query.filter(Ticket.status==status)

        if self._user.role == R_USER_TYPES.get('Cliente'):
            ticket_query = ticket_query.filter(or_(Ticket.assigned_pid == self._user.pid, Ticket.owner_pid == self._user_pid))

        return ticket_query

    def _filter_changed(self):
        date_op     = self._operators.get(self.ui.cmbFormatDate.currentText())\
                      if self.ui.cmbFormatDate.currentText() != '--' else None

        ticket_date = self.ui.dtDatetime.dateTime().toPyDateTime()
        owner       = self.ui.cmbOwner.currentData() if self.ui.cmbOwner.currentText() != 'Sin filtro' else None
        assigned    = self.ui.cmbAssigned.currentData() if self.ui.cmbAssigned.currentText() != 'Sin filtro' else None
        prior       = self.ui.cmbPriority.currentData() if self.ui.cmbPriority.currentText() != 'Sin filtro' else None
        status      = self.ui.cmbStatus.currentData() if self.ui.cmbStatus.currentText() != 'Sin filtro' else None

        ticket_query = self._get_filter(date_op, ticket_date, owner, assigned, prior, status, ss=None)
        self.set_tickets(tickets=ticket_query.all())

    @alchemy_ss
    def _fill_filter_boxes(self, ss=None):
        def _get_users():
            users = ss.query(User.pid, User.fullname).all()
            return users

        for pid, txt in R_TICKET_STATUS.items():
            self.ui.cmbStatus.addItem(pid, txt)

        for pid, txt in R_TICKET_PRIOR.items():
            self.ui.cmbPriority.addItem(pid, txt)

        for pid, txt in _get_users():
            self.ui.cmbOwner.addItem(txt, pid)
            self.ui.cmbAssigned.addItem(txt, pid)

        self.ui.cmbFormatDate.addItems(self._operators.keys())

    def _configure_tables(self):
        columns = ['Identificador', 'Fecha', 'Creador', 'Asignado a', 'Prioridad', 'Estado']
        props   = ['pid', 'created', 'owner', 'assigned', 'priority', 'status']

        self.ui.tblTickets.setSortingEnabled(True)
        self.ui.tblTickets.doubleClicked.connect(self._item_clicked)
        self.ui.tblTickets.set_handler(ticket_handler)
        self.ui.tblTickets.set_header_view(QHeaderView.Stretch)
        self.ui.tblTickets.set_columns(columns, props)

    def _item_clicked(self, model):
        if not self._user.is_cliente() or self._user.is_externo():
            row = model.row()
            ticket = self.ui.tblTickets.getObjectAtRow(row)
            self.ticketSelected.emit(ticket.pid)

    def _btn_filter_released(self):
        visible = self.ui.wdgFiltro.isVisible()
        if not visible:
            self.ui.dtDatetime.setDate(QDate.currentDate())
            self.ui.cmbOwner.setCurrentIndex(0)
            self.ui.cmbAssigned.setCurrentIndex(0)
            self.ui.cmbPriority.setCurrentIndex(0)
            self.ui.cmbStatus.setCurrentIndex(0)

        self.ui.wdgFiltro.setVisible(not visible)
        self._filter_changed()

    def _btn_action(self, action):
        if action == BACK and self._page>0:
            self._page = self._page + action

        elif action == NEXT and self._page<self._max_page:
            self._page = self._page + action

        self.ui.txtPage.setText(str(self._page + 1))
        self.set_tickets()

    def _editing_finished(self):
        txt_number = self.ui.txtPage.text().strip()
        if txt_number.isdigit():
            self._page = int(txt_number) - 1
            self.ui.txtPage.setStyleSheet("border: 1px solid #E0E0E0;\n"
                                          "color: black;")
            self.set_tickets()

        else:
            self.ui.txtPage.setStyleSheet("border: 1px solid red;\n"
                                          "color: black;");

    @alchemy_ss
    def _searching(self, ss=None):
        start, stop = TICKETS_PER_PAGE * self._page, TICKETS_PER_PAGE * (self._page + 1)

        txt = self.ui.txtSearch.text()
        tickets = ss.query(Ticket)\
                    .filter(Ticket.pid.like(txt))

        if self._user.role == R_USER_TYPES.get('Cliente'):
            tickets = tickets.filter(or_(Ticket.assigned_pid == self._user.pid, Ticket.owner_pid == self._user.pid))

        tickets = tickets.offset(start).limit(stop)

        self.ui.tblTickets.clear_contents()
        for ticket in tickets:
            self.ui.tblTickets.addObject(ticket)

    def count(self):
        self._max_page = int(self._count() / TICKETS_PER_PAGE) - 1

    @alchemy_ss
    def _count(self, ss=None):
        co = ss.query(Ticket)

        if self._user.role == R_USER_TYPES.get('Cliente'):
            co = co.filter(or_(Ticket.assigned_pid==self._user.pid, Ticket.owner_pid == self._user.pid))

        return co.count()

    @alchemy_ss
    def _get_tickets(self, page, ss=None):
        start, stop = TICKETS_PER_PAGE * page, TICKETS_PER_PAGE * (page + 1)

        tickets = ss.query(Ticket)

        if self._user.role == R_USER_TYPES.get('Cliente'):
            tickets = tickets.filter(or_(Ticket.assigned_pid == self._user.pid, Ticket.owner_pid == self._user.pid))

        tickets = tickets.offset(start).limit(stop)

        return tickets

    def set_tickets(self, tickets=None):
        if self.ui.txtSearch.text() == '' and not self.ui.wdgFiltro.isVisible():
            self.ui.tblTickets.clear_contents()
            for ticket in self._get_tickets(self._page):
                self.ui.tblTickets.addObject(ticket)

        elif self.ui.wdgFiltro.isVisible():
            self.ui.tblTickets.clear_contents()
            self._page = 0
            self.ui.txtPage.setText('1')
            for ticket in tickets:
                self.ui.tblTickets.addObject(ticket)

        else:
            self._searching()

