from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from threading import Timer
from functools import partial

from sqlalchemy import or_

from src.gui.forms.insert_ticket_page import Ui_Form
from src.storage.session import alchemy_ss
from src.storage.model import Ticket, User
from src.storage.model import R_TICKET_PRIOR, R_TICKET_STATUS, USER_TYPES, R_SCAL_TYPES, R_USER_TYPES, R_KNOWLEDGE


SAVE, SAVE_AND_EXIT = range(2)
BAD_MSG, GOOD_MSG   = range(2)


class InsertTicketPage(QWidget):
    def __init__(self, user, parent=None):
        super(InsertTicketPage, self).__init__(parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self._user = user
        self._scalability = -1

        # Configure
        self._gui_logic()
        self._fill_users()
        self._hide_messages()
        self.ui.dtFecha.setDateTime(QDateTime.currentDateTime())

        self.ui.cmbScalability.setVisible(False)
        self.ui.label_7.setVisible(False)

        self._add_ticket_status()
        self._add_ticket_priority()
        self._add_ticket_scalability()

        self._gui_logic()
        self._bussines_idea()

        self.ui.btnSaveAndExit.released.connect(partial(self._saveit, SAVE_AND_EXIT))
        self.ui.btnSave.released.connect(partial(self._saveit, SAVE))
        self.ui.cmbAsignado.currentIndexChanged.connect(self._asigned_changed)

    def _gui_logic(self):
        if self._user.is_cliente() or self._user.is_externo():
            self.ui.cmbEstado.setVisible(False)
            self.ui.cmbAsignado.setVisible(False)
            self.ui.cmbPrioridad.setVisible(False)
            self.ui.cmbScalability.setVisible(False)

            self.ui.label_3.setVisible(False)
            self.ui.label_4.setVisible(False)
            self.ui.label_5.setVisible(False)
            self.ui.label_7.setVisible(False)

    def _hide_messages(self):
        self.ui.lblBadAlarm.setVisible(False)
        self.ui.lblGoodAlarm.setVisible(False)

    def _show_message(self, msg, mtype):
        lbl = self.ui.lblGoodAlarm if mtype==GOOD_MSG else self.ui.lblBadAlarm
        lbl.setVisible(True)

        lbl.setText(msg)
        Timer(5, self._hide_messages).start()

    def _asigned_changed(self, idx):
        if idx != -1:
            user_pid = self.ui.cmbAsignado.itemData(idx)
            knowledge, is_externo = self._scal_user(user_pid)
            if is_externo:
                self._scalability = R_SCAL_TYPES.get('Externo')

            elif knowledge == 'Básicos':
                self._scalability = R_SCAL_TYPES.get('Básico')

            elif knowledge == 'Avanzados':
                self._scalability = R_SCAL_TYPES.get('Experto')

    @alchemy_ss
    def _scal_user(self, user_pid, ss=None):
        q = ss.query(User).filter(User.pid==user_pid).one()
        return q.get_knowledge(), q.is_externo()

    def _bussines_idea(self):
        if self._user.is_cliente():
            self.ui.cmbEstado.setCurrentText('Asignado')
            self.ui.cmbPrioridad.setCurrentText('Baja')

            assignee = self._get_first_assignee()
            if assignee is None:
                self._show_message(self, 'No se ha encontrado ningún programador', BAD_MSG)
                self.ui.btnSave.setEnabled(False)

            else:
                idx = self.ui.cmbAsignado.findText(assignee.fullname)
                self.ui.cmbAsignado.setCurrentIndex(idx)

    @alchemy_ss
    def _get_first_assignee(self, ss=None):
        programador   = User.role==R_USER_TYPES.get("Programador")
        avanzado      = User.knowledge==R_KNOWLEDGE.get('Avanzados')
        basico        = User.knowledge==R_KNOWLEDGE.get('Básicos')
        administrador = User.role==R_USER_TYPES.get('Administrador')

        scals = [(programador, basico), (programador, avanzado), (administrador,)]
        for scal in scals:
            assignee = ss.query(User).filter(*scal).first()
            if assignee is not None:
                return assignee

        return None

    @alchemy_ss
    def _fill_users(self, ss=None):
        def _get_users():
            q_users = ss.query(User.fullname, User.pid)\
                        .filter(User.role != R_USER_TYPES.get('Cliente'))\
                        .all()

            extern_user = ss.query(User.fullname, User.pid)\
                            .filter(User.extern == True)\
                            .all()

            users  = set([(user[0], user[1]) for user in q_users])
            eusers = set([(user[0], user[1]) for user in extern_user])
            return list(users.union(eusers))

        for fname, pid in _get_users():
            self.ui.cmbAsignado.addItem(fname, pid)

    def _add_ticket_status(self):
        for txt, pid in R_TICKET_STATUS.items():
            self.ui.cmbEstado.addItem(txt, pid)

        self.ui.cmbEstado.setCurrentText('Inicio')

    def _add_ticket_priority(self):
        for txt, pid in R_TICKET_PRIOR.items():
            self.ui.cmbPrioridad.addItem(txt, pid)

        self.ui.cmbPrioridad.setCurrentText('Baja')

    def _add_ticket_scalability(self):
        for txt, pid in R_SCAL_TYPES.items():
            self.ui.cmbScalability.addItem(txt, pid)

        self.ui.cmbPrioridad.setCurrentText('Básico')

    def _saveit(self, mode):
        self._save()
        self.ui.dtFecha.setDateTime(QDateTime.currentDateTime())
        self.ui.cmbEstado.setCurrentText('Inicio')
        self.ui.cmbPrioridad.setCurrentText('Baja')
        self.ui.cmbScalability.setCurrentText('Básico')
        self.ui.txtDescripcion.clear()

    @alchemy_ss
    def _save(self, ss=None):
        try:
            time_and_date = self.ui.dtFecha.dateTime().toPyDateTime()
            user_pid      = self.ui.cmbAsignado.currentData()
            ticket_status = self.ui.cmbEstado.currentData()
            ticket_prior  = self.ui.cmbPrioridad.currentData()
            ticket_scal   = self._scalability
            ticket_desc   = str(self.ui.txtDescripcion.toPlainText())

            ticket = Ticket(created=time_and_date,
                            status=ticket_status, priority=ticket_prior, scalability=ticket_scal,
                            owner_pid=self._user.pid, assigned_pid=user_pid,
                            description=ticket_desc)

            ss.add(ticket)
            ss.commit()

            self._show_message('Se ha insertado correctamente el ticket.', GOOD_MSG)

        except Exception as e:
            from traceback import format_exc
            self._show_message('ERROR: %s' % e, BAD_MSG)

    def reload_users(self):
        self.ui.cmbAsignado.clear()
        self._fill_users()
        self._bussines_idea()
