from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from datetime import datetime

from src.gui.forms.comentario import Ui_Form

from src.storage.session import alchemy_ss
from src.storage.model import Comment


class Comentario(QWidget):
    def __init__(self, parent=None):
        super(Comentario, self).__init__(parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self._now = datetime.now()
        self.ui.txtDate.setText(self._now.strftime('%d/%m/%Y %H:%M'))

    @property
    def commentary(self):
        return self.ui.txtComentario.toPlainText()

    @commentary.setter
    def commentary(self, comment):
        self.ui.txtComentario.appendPlainText(comment)

    @property
    def author(self):
        return self.ui.txtUsuario.text()

    @author.setter
    def author(self, auth):
        self.ui.txtUsuario.setText(auth)

    @property
    def datetime(self):
        return self._now

    @datetime.setter
    def datetime(self, dt):
        self._now = dt
        self.ui.txtDate.setText(dt.strftime('%d/%m/%Y %H%M'))

    def setEnabled(self, boolean):
        self.ui.txtComentario.setEnabled(boolean)

    @alchemy_ss
    def save(self, owner, ticket, ss=None):
        comment            = Comment()
        comment.created    = self._now
        comment.owner_pid  = owner
        comment.text       = self.ui.txtComentario.toPlainText()
        comment.ticket_pid = ticket

        ss.add(comment)
        ss.commit()
