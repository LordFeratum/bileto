from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from src.storage.model import TICKET_STATUS, TICKET_PRIOR, R_USER_TYPES, SCAL_TYPES, R_SCAL_TYPES
from src.storage.model import R_KNOWLEDGE
from src.storage.model import User, Ticket, Comment
from src.storage.session import alchemy_ss

from src.gui.forms.view_ticket_page import Ui_Form
from src.gui.windows.Comentario import Comentario


INFO, CRITICAL = range(2)


class ViewTicketPage(QWidget):
    def __init__(self, user, parent=None):
        super(ViewTicketPage, self).__init__(parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self._user     = user
        self._users    = {}
        self._comments = []
        self._ticket = None
        self._changes  = {}

        self._hide_msg()
        self._fill_scalability()
        self._fill_priority()
        self._fill_status()
        self._fill_users()

        self.ui.cmbAsignado.currentIndexChanged.connect(self._asignado_changed)
        self.ui.cmbEstado.currentIndexChanged.connect(self._status_changed)
        self.ui.cmbPrioridad.currentIndexChanged.connect(self._priority_changed)
        self.ui.cmbScalability.currentIndexChanged.connect(self._scalability_changed)

        self.ui.txtDescripcion.textChanged.connect(self._description_changed)
        self._changes  = {}

        self.ui.btnAddComentario.released.connect(self._add_comment)
        self.ui.btnSave.released.connect(self._btn_save)

    def _bussines_idea(self):
        if self._user.role == R_USER_TYPES.get('Cliente') and not self._user.is_externo():
            self.ui.cmbPrioridad.setEnabled(False)
            self.ui.cmbEstado.setEnabled(False)
            self.ui.cmbAsignado.setEnabled(False)
            self.ui.btnAddComentario.setEnabled(False)
            self.ui.btnSave.setEnabled(False)

        elif self._user.role == R_USER_TYPES.get('Programador') and self._ticket is not None and self._ticket.assigned_pid != self._user.pid:
            cierre_index = self.ui.cmbEstado.findText('Cerrado', Qt.MatchFixedString)
            self.ui.cmbPrioridad.setEnabled(False)
            self.ui.cmbScalability.setEnabled(False)
            self.ui.cmbEstado.removeItem(cierre_index)

        elif self._user.is_externo() and self._ticket is not None:
            self.ui.cmbPrioridad.setEnabled(False)
            self.ui.cmbScalability.setVisible(False)
            self.ui.label_2.setVisible(False)
            self.ui.cmbAsignado.setVisible(False)
            self.ui.label_5.setVisible(False)
            self.ui.txtDescripcion.setEnabled(False)

    def _asignado_changed(self, idx):
        user_pid = self.ui.cmbAsignado.currentData()
        user_name = self.ui.cmbAsignado.currentText()
        self._changes.update({'assigned': (user_pid, user_name)})

    def _scalability_changed(self, idx):
        scal_pid = self.ui.cmbScalability.currentData()
        scal_txt = self.ui.cmbScalability.currentText()
        self._changes.update({'scalability': (scal_pid, scal_txt)})
        self._do_scalability(scal_txt, scal_pid)

    def _status_changed(self, idx):
        status_pid = self.ui.cmbEstado.currentData()
        status_txt = self.ui.cmbEstado.currentText()
        self._changes.update({'status': (status_pid, status_txt)})

    def _priority_changed(self, idx):
        prior_pid = self.ui.cmbPrioridad.currentData()
        prior_txt = self.ui.cmbPrioridad.currentText()
        self._changes.update({'priority': (prior_pid, prior_txt)})

    def _description_changed(self):
        self._changes.update({'description': ('', self.ui.txtDescripcion.toPlainText())})

    @alchemy_ss
    def _do_scalability(self, scal, scal_pid, ss=None):
        programador = User.role == R_USER_TYPES.get('Programador')
        externo     = User.extern == True
        admin       = User.role == R_USER_TYPES.get('Administrador')
        basico      = User.knowledge == R_KNOWLEDGE.get('Básicos')
        avanzado    = User.knowledge == R_KNOWLEDGE.get('Avanzados')

        if scal_pid != self._ticket.scalability:
            fn = None
            if scal == 'Externo':
                fn = ss.query(User.fullname).filter(externo).first()

            elif scal == 'Experto':
                fn = ss.query(User.fullname).filter(programador, avanzado).first()

            elif scal == 'Básico':
                fn = ss.query(User.fullname).filter(programador, basico).first()

            if fn is not None:
                self.ui.cmbAsignado.setCurrentText(fn[0])

            else:
                admin = ss.query(User.fullname).filter(admin).first()
                if admin is not None:
                    self.ui.cmbAsignado.setCurrentText(fn[0])

                else:
                    self._show_message('No se ha encontrado ningún administrador.', CRITICAL)

    def _hide_msg(self):
        self.ui.lblBad.setVisible(False)
        self.ui.lblGood.setVisible(False)

    def _show_message(self, msg, mtype):
        if mtype == INFO:
            self.ui.lblGood.setVisible(True)
            self.ui.lblGood.setText(msg)

        else:
            self.ui.lblBad.setVisible(True)
            self.ui.lblBad.setText(msg)

        QTimer.singleShot(5000, self._hide_msg)

    def _fill_scalability(self):
        for pid, text in SCAL_TYPES.items():
            self.ui.cmbScalability.addItem(text, pid)

        self._changes = {}

    def _fill_status(self):
        for pid, text in TICKET_STATUS.items():
            self.ui.cmbEstado.addItem(text, pid)

        self._changes = {}

    def _fill_priority(self):
        for pid, text in TICKET_PRIOR.items():
            self.ui.cmbPrioridad.addItem(text, pid)

        self._changes = {}

    @alchemy_ss
    def _fill_users(self, ss=None):
        users = ss.query(User.fullname, User.pid).all()
        for username, pid in users:
            self._users.update({pid: username})
            self.ui.cmbAsignado.addItem(username, pid)

    @alchemy_ss
    def _fill_comments(self, ss=None):
        comments = ss.query(Comment)\
                     .filter(Comment.ticket_pid==self._ticket.pid)\
                     .all()

        _comments = comments if comments is not None else []

        self.ui.lstComentarios.clear()
        for comm in _comments:
            commentary = Comentario()
            commentary.datetime   = comm.created
            commentary.author     = comm.owner.fullname
            commentary.commentary = comm.text
            commentary.setEnabled(False)

            item = QListWidgetItem()
            item.setSizeHint(commentary.sizeHint())

            self.ui.lstComentarios.addItem(item)
            self.ui.lstComentarios.setItemWidget(item, commentary)

    @alchemy_ss
    def _btn_save(self, ss=None):
        for commentary in self._comments:
            commentary.save(self._user.pid, self._ticket.pid)
            commentary.setEnabled(False)

        message = []
        for ctype, (change_pid, change_txt) in self._changes.items():
            if ctype == 'assigned':
                message.append('Ha asignado el ticket a %s.' % (change_txt))
                self._ticket.assigned_pid = change_pid

            elif ctype == 'status':
                message.append('Ha cambiado el estado a %s.' % (change_txt))
                self._ticket.status = change_pid

            elif ctype == 'priority':
                message.append('Ha cambiado la prioridad a %s.' % (change_txt))
                self._ticket.priority = change_pid

            elif ctype == 'scalability':
                message.append('Ha cambiado la dificultad a %s.' % (change_txt))
                self._ticket.scalability = change_pid

            elif ctype == 'description':
                message.append('Ha cambiado la descripción.')
                self._ticket.description = change_txt

        ss.add(self._ticket)
        ss.commit()

        self._comments = []
        txt_msg = '\n'.join(message)
        if txt_msg:
            self._add_comment(msg=txt_msg, status=False)
            self._comments[0].save(self._user.pid, self._ticket.pid)

        self._show_message('Se ha modificado el ticket #%d' % self._ticket.pid, INFO)
        self._changes = {}

    def _add_comment(self, msg=None, status=True):
        commentary = Comentario()
        commentary.author = self._user.fullname
        commentary.setEnabled(status)

        if msg is not None:
            commentary.commentary = msg

        item = QListWidgetItem()
        item.setSizeHint(commentary.sizeHint())

        self.ui.lstComentarios.addItem(item)
        self.ui.lstComentarios.setItemWidget(item, commentary)

        self._comments.append(commentary)

    def set_ticket(self, ticket):
        self._comments = []

        self._ticket = ticket
        self.ui.lblTicket.setText('#%s' %str(ticket.pid))
        self.ui.lblDate.setText(str(ticket.created.strftime('%d/%m/%Y  (%H:%M)')))
        self.ui.txtDescripcion.setPlainText(ticket.description)

        self.ui.cmbScalability.setCurrentText(SCAL_TYPES.get(ticket.scalability))
        self.ui.cmbEstado.setCurrentText(TICKET_STATUS.get(ticket.status))
        self.ui.cmbPrioridad.setCurrentText(TICKET_PRIOR.get(ticket.priority))
        self.ui.cmbAsignado.setCurrentText(self._users.get(ticket.assigned_pid))

        self._bussines_idea()

        self._changes = {}

        self._fill_comments()
