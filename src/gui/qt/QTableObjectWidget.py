from PyQt5 import QtCore, QtGui, QtWidgets

class QTableObjectWidget(QtWidgets.QTableWidget):
    def __init__(self, columns=[], props=[], header_view=None, handler=None):
        QtWidgets.QTableWidget.__init__(self)
        self._columns     = columns
        self._props       = props
        self._header_view = header_view
        self._handler     = handler

        self._rows = 0
        self._elems = {}

        self._set_columns()

    def set_header_view(self, header):
        self._header_view = header

    def set_columns(self, columns, props):
        self._columns = columns
        self._props   = props

        self._set_columns()

    def set_handler(self, hdl):
        self._handler = hdl

    def clear_contents(self):
        self.clear()
        self._rows = 0
        self.setRowCount(0)
        self._set_columns()

    def _set_columns(self):
        if self._columns:
            self.setColumnCount(len(self._columns))
            self.setHorizontalHeaderLabels(self._columns)

        if self._header_view is not None:
            self.horizontalHeader().setSectionResizeMode(self._header_view)

    def addObject(self, obj):
        self.insertRow(self._rows)
        self._elems.update({self._rows: obj})

        for column, elem in enumerate(self._props):
            if self._handler is None:
                attr = getattr(obj, elem)

            else:
                attr = self._handler(obj, elem)

            item = QtWidgets.QTableWidgetItem()
            item.setData(QtCore.Qt.EditRole, attr)
            if not isinstance(attr, str):
                attr = str(attr)

            item.setData(QtCore.Qt.DisplayRole, attr)
            self.setItem(self._rows, column, item)

        self._rows += 1

    def getObjectAtRow(self, row):
        return self._elems.get(row)

    def removeObjectAtRow(self, idx):
        self.removeRow(idx)
        self._elems.pop(idx, None)


class Obj(object):
    def __init__(self, a, b, c):
        self._a = a
        self._b = b
        self._c = c

    @property
    def a(self):
        return self._a

    @property
    def b(self):
        return self._b

    @property
    def c(self):
        return self._c


def main(args):
    app = QtWidgets.QApplication(args)
    table = QTableObjectWidget(columns=['Col1', 'Col2', 'Col3'], props=['a', 'b', 'c'], header_view=QtWidgets.QHeaderView.Stretch)

    algo = Obj(1, 2, 3)
    table.addObject(algo)

    table.show()
    sys.exit(app.exec_())

if __name__=="__main__":
    import sys
    main(sys.argv)
