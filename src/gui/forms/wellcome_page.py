# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui/forms/wellcome_page.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from src.gui.qt.QTableObjectWidget import QTableObjectWidget

class Ui_WelcomePage(object):
    def setupUi(self, WelcomePage):
        WelcomePage.setObjectName("WelcomePage")
        WelcomePage.resize(1074, 749)
        WelcomePage.setStyleSheet("")
        self.verticalLayout = QtWidgets.QVBoxLayout(WelcomePage)
        self.verticalLayout.setContentsMargins(50, 30, 50, 25)
        self.verticalLayout.setSpacing(13)
        self.verticalLayout.setObjectName("verticalLayout")
        self.lblMessage = QtWidgets.QLabel(WelcomePage)
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(20)
        self.lblMessage.setFont(font)
        self.lblMessage.setStyleSheet("color: rgb(69, 72, 87);")
        self.lblMessage.setObjectName("lblMessage")
        self.verticalLayout.addWidget(self.lblMessage)
        self.tblTicketPendientes = QTableObjectWidget()
        self.tblTicketPendientes.setStyleSheet("QHeaderView::section{\n"
"    color: black;\n"
"}\n"
"QTableWidget{"
"    color: black;\n"
"}")
        self.tblTicketPendientes.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.tblTicketPendientes.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.tblTicketPendientes.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.tblTicketPendientes.setGridStyle(QtCore.Qt.CustomDashLine)
        self.tblTicketPendientes.setRowCount(0)
        self.tblTicketPendientes.setColumnCount(3)
        self.tblTicketPendientes.setObjectName("tblTicketPendientes")
        item = QtWidgets.QTableWidgetItem()
        self.tblTicketPendientes.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tblTicketPendientes.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tblTicketPendientes.setHorizontalHeaderItem(2, item)
        self.tblTicketPendientes.horizontalHeader().setStretchLastSection(True)
        self.verticalLayout.addWidget(self.tblTicketPendientes)

        self.retranslateUi(WelcomePage)
        QtCore.QMetaObject.connectSlotsByName(WelcomePage)

    def retranslateUi(self, WelcomePage):
        _translate = QtCore.QCoreApplication.translate
        WelcomePage.setWindowTitle(_translate("WelcomePage", "Form"))
        self.lblMessage.setText(_translate("WelcomePage", "Tickets Pendientes"))
        self.tblTicketPendientes.setSortingEnabled(True)
        item = self.tblTicketPendientes.horizontalHeaderItem(0)
        item.setText(_translate("WelcomePage", "New Column 1"))
        item = self.tblTicketPendientes.horizontalHeaderItem(1)
        item.setText(_translate("WelcomePage", "New Column 2"))
        item = self.tblTicketPendientes.horizontalHeaderItem(2)
        item.setText(_translate("WelcomePage", "New Column 3"))

