# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'src/gui/forms/comentario.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(931, 94)
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.widget = QtWidgets.QWidget(Form)
        self.widget.setMinimumSize(QtCore.QSize(0, 20))
        self.widget.setMaximumSize(QtCore.QSize(16777215, 20))
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setContentsMargins(0, -1, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.txtUsuario = QtWidgets.QLabel(self.widget)
        self.txtUsuario.setMinimumSize(QtCore.QSize(0, 10))
        self.txtUsuario.setMaximumSize(QtCore.QSize(16777215, 10))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(8)
        self.txtUsuario.setFont(font)
        self.txtUsuario.setStyleSheet("color: black;")
        self.txtUsuario.setObjectName("txtUsuario")
        self.horizontalLayout.addWidget(self.txtUsuario)
        self.line = QtWidgets.QFrame(self.widget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.horizontalLayout.addWidget(self.line)
        spacerItem = QtWidgets.QSpacerItem(742, 8, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.txtDate = QtWidgets.QLabel(self.widget)
        self.txtDate.setMinimumSize(QtCore.QSize(0, 10))
        self.txtDate.setMaximumSize(QtCore.QSize(16777215, 10))
        font = QtGui.QFont()
        font.setFamily("Arial")
        font.setPointSize(8)
        self.txtDate.setFont(font)
        self.txtDate.setStyleSheet("color: black;")
        self.txtDate.setObjectName("txtDate")
        self.horizontalLayout.addWidget(self.txtDate)
        self.verticalLayout.addWidget(self.widget)
        self.txtComentario = QtWidgets.QPlainTextEdit(Form)
        self.txtComentario.setEnabled(True)
        self.txtComentario.setMinimumSize(QtCore.QSize(0, 50))
        self.txtComentario.setMaximumSize(QtCore.QSize(16777215, 50))
        self.txtComentario.setStyleSheet("color: black;")
        self.txtComentario.setObjectName("txtComentario")
        self.verticalLayout.addWidget(self.txtComentario)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.txtUsuario.setText(_translate("Form", "Enrique Piña"))
        self.txtDate.setText(_translate("Form", "2016-01-01 20:41"))

