
class UserNotFoundException(Exception):
    def __init__(self, message):
        super(UserNotFoundException, self).__init__(message)
        self._message = message

    @property
    def message(self):
        return self._message

    @message.setter
    def message(self, msg):
        self._message = msg
